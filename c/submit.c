#define _GNU_SOURCE

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define DEFAULT_SOCKET_PATH "/run/spamdb/socket"

#define SOCKET_TIMEOUT_SECONDS 5

#define TYPE_UNKNOWN 0
#define TYPE_SPAM    1
#define TYPE_HAM     2

#define BUFSZ 4096

static void usage() {
  fprintf(stderr,
          "Usage: spamdbc [<options>] {--ham|--spam}\n"
          "\n"
          "Reads an email message on standard input and forwards it to the\n"
          "local spamdb relay over a UNIX socket.\n"
          "\n"
          "Options:\n"
          "\n"
          "  -h, --help       Show this message\n"
          "  --socket=PATH    Connect to the specified socket (default " DEFAULT_SOCKET_PATH ")\n"
          "  --ham            Classify the message as non-spam\n"
          "  --spam           Classify the message as spam\n"
          "\n");
}

static int submit(const char *socket_path, int type) {
  struct sockaddr_un addr;
  struct timeval timeout;
  int sock, r;
  char buf[BUFSZ];
  
  memset(&addr, 0, sizeof(struct sockaddr_un));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);

  if ((sock = socket(PF_UNIX, SOCK_STREAM, 0)) < 0) {
    fprintf(stderr, "error opening socket: %s\n", strerror(errno));
    return -1;
  }

  timeout.tv_sec = SOCKET_TIMEOUT_SECONDS;
  timeout.tv_usec = 0;
  if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0) {
    goto err;
  }
  if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)) < 0) {
    goto err;
  }
  
  if (connect(sock, (const struct sockaddr *)&addr, sizeof(addr)) < 0) {
    goto err;
  }

  // Write the request.
  if (type == TYPE_HAM) {
    r = send(sock, "ham\n", 4, MSG_NOSIGNAL);
  } else {
    r = send(sock, "spam\n", 5, MSG_NOSIGNAL);
  }
  if (r < 0) {
    goto err;
  }

  // Copy stdin to the socket.
  while (1) {
    int n = read(0, buf, BUFSZ);
    if (n < 0)
      break;
    n = send(sock, buf, n, MSG_NOSIGNAL);
    if (n < 0)
      goto err;
  }

  close(sock);
  return 0;

 err:
  close(sock);
  fprintf(stderr, "socket error: %s\n", strerror(errno));
  return -1;
}

int main(int argc, char **argv) {
  int c;
  int type = TYPE_UNKNOWN;
  const char *socket_path = DEFAULT_SOCKET_PATH;

  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
                                           {"help", no_argument, 0, 'h'},
                                           {"ham", no_argument, 0, 0},
                                           {"spam", no_argument, 0, 0},
                                           {"socket", required_argument, 0, 0},
                                           {0, 0, 0, 0}
    };

    c = getopt_long(argc, argv, "h", long_options, &option_index);
    if (c == -1)
      break;
    switch (c) {
    case 0:
      switch (option_index) {
      case 0:
        type = TYPE_HAM;
        break;
      case 1:
        type = TYPE_SPAM;
        break;
      case 2:
        socket_path = optarg;
        break;
      }
      break;
    case 'h':
      usage();
      exit(0);
    default:
      usage();
      exit(2);
    }
  }

  if (optind < argc) {
    fprintf(stderr, "Too many arguments\n");
    usage();
    exit(2);
  }

  if (type == TYPE_UNKNOWN) {
    fprintf(stderr, "Must specify one of --spam / --ham\n");
    usage();
    exit(2);
  }

  if (submit(socket_path, type) < 0) {
    exit (1);
  }

  return 0;
}

package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/exec"

	"git.autistici.org/ai3/go-common/clientutil"
	"github.com/google/subcommands"
)

type dumpConfig struct {
	Upstream *clientutil.BackendConfig `yaml:"upstream"`
}

type dumpCommand struct {
	config dumpConfig

	configPath string
}

func (c *dumpCommand) Name() string     { return "dump" }
func (c *dumpCommand) Synopsis() string { return "dump dataset" }
func (c *dumpCommand) Usage() string {
	return `dump [<flags>]:
        Dump the spamassassin bayes dataset.

`
}

func (c *dumpCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.configPath, "config", "/etc/spamdb/client.yml", "configuration file `path`")
}

func (c *dumpCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	if err := loadYAML(c.configPath, &c.config); err != nil {
		log.Printf("error loading configuration: %v", err)
		return subcommands.ExitUsageError
	}

	be, err := clientutil.NewBackend(c.config.Upstream)
	if err != nil {
		log.Printf("error creating connection to server: %v", err)
		return subcommands.ExitFailure
	}

	resp, err := be.Get(ctx, "/export/bayes.lz4", "")
	if err != nil {
		log.Printf("error downloading dataset: %v", err)
		return subcommands.ExitFailure
	}
	defer resp.Body.Close()

	cmd := exec.Command("lz4c", "-d", "-c")
	cmd.Stdin = resp.Body
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		log.Printf("error decompressing dataset: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&dumpCommand{}, "")
}
